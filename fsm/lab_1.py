from string import ascii_lowercase
from random import choice


class BaseCollection(object):

    def __init__(self, items):
        self.items = items

    def add(self, item):
        if item not in self.items:
            self.items.append(item)

    def remove(self, item):
        if item in self.items:
            self.items.remove(item)

    def __iter__(self):
        for item in self.items:
            yield item

    def __getitem__(self, key):
        try:
            return self.items[key]
        except IndexError:
            return

    def index(self, item):
        try:
            return self.items.index(item)
        except ValueError:
            return


class Alphabet(BaseCollection):

    @classmethod
    def make(cls, size, source=ascii_lowercase):
        word = ''.join([choice(source) for _ in range(size)])
        return cls(list(dict.fromkeys(word)))

    def __contains__(self, items):
        return all(item in self.items for item in items)

    def __str__(self):
        return ''.join(self.items)


class Dictionary(BaseCollection):

    def __init__(self, alphabet, words):
        self.alphabet = alphabet
        super().__init__(self.__clean_words(words))

    def __clean_words(self, words):
        return [word for word in words if word in self.alphabet]

    @classmethod
    def make(cls, dict_size, word_size, alphabet):
        words = [Alphabet.make(word_size, str(alphabet)) for _ in range(dict_size)]
        return cls(alphabet, words)

    def __str__(self):
        return ', '.join([str(item) for item in self.items])

    def sort(self):
        self.items.sort(key=lambda w: [self.alphabet.index(c) for c in w])


alphabet = Alphabet.make(10)
print('Alphabet:', alphabet)

symbol1 = Alphabet.make(1, str(alphabet))
symbol2 = Alphabet.make(1)
print('symbol {}:'.format(symbol1), str(symbol1) in alphabet)
print('symbol {}:'.format(symbol2), str(symbol2) in alphabet)

word1 = Alphabet.make(4, str(alphabet))
word2 = Alphabet.make(3)
print('word {}:'.format(word1), word1 in alphabet)
print('word {}:'.format(word2), word2 in alphabet)

tmp = alphabet[0]
print('before delete:', alphabet)
alphabet.remove(tmp)
print('after delete symbol {}: '.format(tmp), alphabet)
alphabet.add(tmp)
print('after adding symbol {}: '.format(tmp), alphabet)

print('=' * 35)
dictionary = Dictionary.make(dict_size=7, word_size=5, alphabet=alphabet)
print('Dictionary:', dictionary)
print('Index element {}:'.format(dictionary[3]), dictionary.index(dictionary[3]))
print('Index element {}:'.format(word1), dictionary.index(word1))

tmp = dictionary[0]
print('before delete:\n', dictionary)
dictionary.remove(tmp)
print('after delete word: {}\n'.format(tmp), dictionary)
dictionary.add(tmp)
print('after adding word: {}\n'.format(tmp), dictionary)

print('before sort:\n', dictionary)
dictionary.sort()
print('after sort:\n', dictionary)
