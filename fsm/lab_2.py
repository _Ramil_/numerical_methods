TYPE_0, CONTEXT_DEPEND, CONTEXT_FREE, RIGHT, LEFT, SIMPLE = range(6)

TYPES = [
    'Грамматика типа 0',
    'Контекстно-зависимая',
    'Контекстно-свободная',
    'Праволинейная',
    'Леволинейная',
    'Простейшая'
]


class Rule:
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.th = None

    def analyze(self, N, V):
        if len(self.left) == 1:

            if len(self.right) <= 1:
                return SIMPLE

            if len(self.right) == 2:
                if self.right in V:
                    return SIMPLE
                if self.right[0] in N and self.right[1] in V:
                    return LEFT
                if self.right[1] in N and self.right[0] in V:
                    return RIGHT
            return CONTEXT_FREE
        else:
            A = set(self.left) - set(V)
            if len(A) == 1:
                number = self.left.find(str(A))
                a = self.left[:number - 2]
                b = self.left[number - 1:]
                if self.right.startswith(a) and self.right.endswith(b):
                    return CONTEXT_DEPEND
            return TYPE_0

    def apply(self, word, position=1):
        i = [n for n in range(len(word)) if word.find(self.left, n) == n]
        if position <= len(i):
            return word[:i[position - 1]] + self.right + word[i[position - 1] + len(self.left):]
        return word

    def __eq__(self, other):
        return self.left == other.left and self.right == other.right


class Grammar:
    def __init__(self, N, V, P, S):
        self.N = N
        self.V = V
        self.P = P
        self.S = S

    def add_rule(self, rule):
        if ((set(rule.left) | set(rule.right)) - (self.N | self.V)) == set():
            self.P.append(rule)

    def remove_rule(self, rule):
        if rule in self.P:
            self.P.remove(rule)

    def show(self):
        print(self.N)
        print(self.V)
        print(self.S)
        for rule in self.P:
            print(rule.left + '->' + rule.right)

    def analyze(self):
        for rule in self.P:
            rule.h = rule.analyze(self.N, self.V)
        h = [rule.h for rule in self.P]
        result = min(h)
        if result == CONTEXT_FREE and LEFT in h:
            result = LEFT
        return TYPES[result]


non_terminal = {'S'}
terminal = {'a'}
rules = [Rule('S', 'aSa'), Rule('S', 'a'), Rule('S', '')]

G = Grammar(non_terminal, terminal, rules, 'S')
G.add_rule(Rule('S', 'aa'))
G.show()
G.remove_rule(Rule('S', 'aa'))
G.show()

word = G.S
word = rules[0].apply(word)
print(word)
word = rules[0].apply(word, 1)
print(word)
word = rules[2].apply(word)
print(word)
G.show()
print(G.analyze())

non_terminal = {'S', 'A', 'Z'}
terminal = {'a', 'b', ''}
rules = [Rule('S', 'Sa'), Rule('S', 'Aa'), Rule('A', 'Ab'), Rule('A', 'Zb'), Rule('Z', '')]
print(Grammar(non_terminal, terminal, rules, 'S').analyze())

non_terminal = {'S', 'A'}
terminal = {'a', 'b', 'c', 'd'}
rules = [Rule('S', 'aBcc'), Rule('B', 'A'), Rule('Ac', 'B'), Rule('A', 'AAA')]
print(Grammar(non_terminal, terminal, rules, 'S').analyze())

non_terminal = {'S', 'A'}
terminal = {'a', 'b'}
rules = [Rule('S', 'A'), Rule('A', 'b')]
print(Grammar(non_terminal, terminal, rules, 'S').analyze())

non_terminal = {'S', 'A', 'Z'}
terminal = {'a', 'b'}
rules = [Rule('S', 'aS'), Rule('aSa', 'abaSa')]
print(Grammar(non_terminal, terminal, rules, 'S').analyze())
