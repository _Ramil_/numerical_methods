from collections import defaultdict


class FSM(object):

    def __init__(self, state):
        self.states = {
            'INITIAL': state,
            'CURRENT': state,
            'STATES':  defaultdict(lambda: defaultdict(lambda: {}))
        }

    def __make_state(self, next_step, action=None):
        return {'ACTION': action or (lambda: ''), 'NEXT': next_step}

    def set_initial(self, symbol, next_step, action=None):
        self.states['INITIAL'][symbol] = self.__make_state(next_step, action)

    def set_current(self, symbol, next_step, action=None):
        self.states['CURRENT'][symbol] = self.__make_state(next_step, action)

    @property
    def current(self):
        return self.states['CURRENT']

    def reset(self):
        self.states['CURRENT'] = self.states['INITIAL']
        self.states['STATES'] = {}

    def add_state(self, state, symbol, next_step, action=None):
        self.states['STATES'][state][symbol] = self.__make_state(next_step, action)

    def remove_state(self, state, symbol):
        try:
            del self.states['STATES'][state][symbol]
        except KeyError:
            pass

    def process(self, symbol):
        current = self.states['CURRENT']
        states = self.states['STATES'][current]
        state = states.get(symbol.upper())

        if not state:
            state = states.get('*')
            if not state:
                raise Exception('Invalid symbol!')

        state.get('ACTION')()
        self.states['CURRENT'] = state.get('NEXT')


fsm = FSM('INIT')
fsm.add_state('INIT', '*', 'INIT', lambda: print('Hello my friend'))
fsm.add_state('INIT', 'LOGIN', 'SESSION', lambda: print('Welcome'))
fsm.add_state("SESSION", "*", "SESSION", lambda: print('Tell me something interesting'))
fsm.add_state("SESSION", "SAY", "SESSION", lambda: print('It\'s very interesting'))
fsm.add_state("SESSION", "DELETE_STATE", "SESSION", lambda: print('I have to be deleted'))
fsm.add_state("SESSION", "EXIT", "INIT")

fsm.remove_state('SESSION', 'DELETE_STATE')
for _symbol in ['hello', 'login', 'nice fsm', 'say', 'exit']:
    fsm.process(_symbol)
