class Rule:
    def __init__(self, state, char, next_state):
        self.state = state
        self.char = char
        self.next_state = next_state

    def is_applies(self, state, char):
        return self.state == state and self.char == char

    def follow(self):
        return self.next_state

    def __str__(self):
        return " ".join((str(self.state), str(self.char), str(self.next_state)))

    def __eq__(self, other):
        return (self.state == other.state and
                self.char == other.char and
                self.next_state == other.next_state)


class RuleBook:
    def __init__(self, rules):
        self.rules = rules

    def next_states(self, states, char):
        new_states = set()
        for state in states:
            for rule in self.rules:
                if rule.is_applies(state, char):
                    new_states.add(rule.follow())
        return new_states

    def __str__(self):
        return "\n".join([str(rule) for rule in self.rules])


class NFA:
    def __init__(self, cur_states, accept_states, rules):
        self.cur_states = cur_states
        self.accept_states = accept_states
        self.rulebook = rules

    def is_accepting(self):
        return bool(self.accept_states.intersection(self.cur_states))

    def read_char(self, ch):
        self.cur_states = self.rulebook.next_states(self.cur_states, ch)

    def read_str(self, word):
        for ch in word:
            self.read_char(ch)


class NFADesign:
    def __init__(self, start_states, accept_states, rules):
        self.start_states = start_states
        self.accept_states = accept_states
        self.rulebook = rules

    def to_nfa(self):
        return NFA(set(self.start_states), set(self.accept_states), self.rulebook)

    def is_accept(self, word):
        nfa = self.to_nfa()
        nfa.read_str(word)
        return nfa.is_accepting()

INIT, SESSION, AUTH, LOGOUT = 'INIT', 'SESSION', 'AUTH', 'LOGOUT'

rulebook = RuleBook([
    Rule(INIT, 'a', INIT),
    Rule(INIT, 'b', INIT),
    Rule(INIT, 'b', SESSION),
    Rule(SESSION, 'a', AUTH),
    Rule(SESSION, 'b', AUTH),
    Rule(AUTH, 'b', LOGOUT),
    Rule(AUTH, 'a', LOGOUT)
])

dnfa = NFADesign([INIT], [LOGOUT], rulebook)
print(dnfa.is_accept("ababaa"))
