S1, S2, START, END = 1, 2, '$', ''


class FASM:
    def __init__(self, cur_config, accept_states, rulebook):
        self.cur_config = cur_config
        self.accept_states = accept_states
        self.rulebook = rulebook

    def read_char(self, char):
        self.cur_config = self.rulebook.next_config(self.cur_config, char)

    def is_accept(self):
        return bool(self.cur_config and
                    self.cur_config.state in self.accept_states)

    def read_string(self, word):
        for char in word:
            self.read_char(char)
            if self.cur_config:
                continue
        self.read_char(END)


class SMConfiguration:
    def __init__(self, state, stack):
        self.state = state
        self.stack = stack


class FASMRule:
    def __init__(self, state, char, next_state, pop_char, push_chars):
        self.state = state
        self.char = char
        self.next_state = next_state
        self.pop_char = pop_char
        self.push_chars = push_chars

    def is_applies(self, config, char):
        return bool(
            config and
            self.state == config.state and
            self.pop_char == config.stack[-1] and
            self.char == char
        )

    def follow(self, config):
        return SMConfiguration(self.next_state, self.next_stack(config))

    def next_stack(self, config):
        config.stack.pop()
        for char in self.push_chars:
            config.stack.append(char)
        return config.stack


class FASMRuleBook:
    def __init__(self, rules):
        self.rules = rules

    def next_config(self, config, char):
        rule = self.rule_for(config, char)
        if rule:
            return rule.follow(config)

    def rule_for(self, config, char):
        return next((r for r in self.rules if r.is_applies(config, char)), None)


alphabet = [['()', '(', ')'], ['[]', '[', ']'], ['{}', '{', '}']]
rules = [FASMRule(S2, END, S1, START, [START])]

for item in alphabet:
    symbol, left, right = item
    rules.append(FASMRule(S1, left, S2, START, [START, symbol]))
    rules.append(FASMRule(S2, left, S2, symbol, [symbol, symbol]))
    rules.append(FASMRule(S2, right, S2, symbol, []))
    rules.append(FASMRule(S2, left, S2, START, [START, symbol]))

    for n_symbol, n_left, n_right in filter(lambda i: i != item, alphabet):
        rules.append(FASMRule(S2, n_left, S2, symbol, [symbol, n_symbol]))

rulebook = FASMRuleBook(rules)
dpda = FASM(SMConfiguration(S1, [START]), [S1], rulebook)
dpda.read_string('[{}]')
print(dpda.is_accept())
