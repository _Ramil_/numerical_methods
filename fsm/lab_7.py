import operator

NUMBER, PLUS, MINUS, MUL, DIV, LPAREN, RPAREN, EOF = (
    'NUMBER', 'PLUS', 'MINUS', 'MUL', 'DIV', '(', ')', 'EOF'
)


class Lexeme:

    def __init__(self, type, value):
        self.type = type
        self.value = value


class Number:

    def __init__(self, lexeme):
        self.lexeme = lexeme
        self.value = lexeme.value


class Expression:

    def __init__(self, left, operand, right):
        self.left = left
        self.operand = operand
        self.right = right


class LexemeParser:

    def __init__(self, text):
        self.text = text
        self.pos = 0
        self.current = self.text[self.pos]
        self.lexeme_map = {
            '+': Lexeme(PLUS, '+'),
            '-': Lexeme(MINUS, '-'),
            '*': Lexeme(MUL, '*'),
            ':': Lexeme(DIV, ':'),
            '(': Lexeme(LPAREN, '('),
            ')': Lexeme(RPAREN, ')')
        }

    def next(self):
        self.pos += 1
        try:
            self.current = self.text[self.pos]
        except IndexError:
            self.current = None

    def number(self):
        result = ''
        while self.current is not None and self.current.isdigit():
            result += self.current
            self.next()
        return int(result)

    def get_lexeme(self):
        while self.current is not None:
            if self.current.isspace():
                self.next()
                continue

            if self.current.isdigit():
                return Lexeme(NUMBER, self.number())

            lexeme = self.lexeme_map.get(self.current)
            if not lexeme:
                raise Exception('Invalid lexeme: {}'.format(self.current))
            self.next()
            return lexeme

        return Lexeme(EOF, None)


class Parser:

    def __init__(self, parser):
        self.lexeme_parser = parser
        self.current = self.lexeme_parser.get_lexeme()

    def check(self, lexeme_type):
        if self.current.type != lexeme_type:
            raise Exception('Invalid lexeme_type: {}'.format(lexeme_type))
        self.current = self.lexeme_parser.get_lexeme()

    def paren_numbers(self):
        lexeme = self.current
        if lexeme.type == NUMBER:
            self.check(NUMBER)
            return Number(lexeme)
        elif lexeme.type == LPAREN:
            self.check(LPAREN)
            node = self.expression()
            self.check(RPAREN)
            return node

    def multipliers(self):
        node = self.paren_numbers()
        while self.current.type in (MUL, DIV):
            lexeme = self.current
            if lexeme.type == MUL:
                self.check(MUL)
            elif lexeme.type == DIV:
                self.check(DIV)
            node = Expression(
                left=node,
                operand=lexeme,
                right=self.paren_numbers()
            )
        return node

    def expression(self):
        node = self.multipliers()

        while self.current.type in (PLUS, MINUS):
            lexeme = self.current
            if lexeme.type == PLUS:
                self.check(PLUS)
            elif lexeme.type == MINUS:
                self.check(MINUS)

            node = Expression(
                left=node,
                operand=lexeme,
                right=self.multipliers()
            )

        return node

    def parse(self):
        return self.expression()

    def render(self, tree, size=0):
        if isinstance(tree.left, Expression):
            self.render(tree.left, size + 2)
        if isinstance(tree.left, Number):
            print('\t' * size, tree.left.value)
        print('\t' * size, tree.operand.value)
        if isinstance(tree.right, Expression):
            self.render(tree.right, size + 2)
        if isinstance(tree.right, Number):
            print('\t' * size, tree.right.value)


class Interpreter:

    def calculate(self, node):
        method = type(node).__name__.lower()
        return getattr(self, method)(node)

    def expression(self, node):
        operation_map = {
            PLUS: operator.add,
            MINUS: operator.sub,
            MUL: operator.mul,
            DIV: operator.truediv,
        }
        operation = operation_map.get(node.operand.type)
        return operation(self.calculate(node.left), self.calculate(node.right))

    def number(self, node):
        return node.value

"""x*(y+z-y1)+w*x2:w3"""

text = "7*(1+2-3)+40*6:6"
parser = Parser(LexemeParser(text))
tree = parser.parse()
print('tree', '=' * 45)
parser.render(tree)
print('tree', '=' * 45)

print('Result: ', Interpreter().calculate(tree))
