import matplotlib.pyplot as plt
import matplotlib.lines as mlines

"""
  A1 * x + B1 * y = C1
  A2 * x + B2 * y = C2
  --------------------
  x = Dx/D
  y = Dy/D
  --------------------
  D = | A1 B1 | == 0?
      | A2 B2 |
  --------------------
  Dx = | C1 B1 |
       | C2 B2 |
  --------------------
  Dy = | A1 C1 |
       | A2 C2 |
"""


def is_paral(a, b, c, d):
    def line(p1, p2):
        A = (p1[1] - p2[1])
        B = (p2[0] - p1[0])
        C = (p1[0] * p2[1] - p2[0] * p1[1])
        return A, B, -C

    L1 = line(a, b)
    L2 = line(c, d)
    D = L1[0] * L2[1] - L1[1] * L2[0]
    return bool(D == 0)


points = [
    [(1, 1), (5, 6)],
    [(6, 1), (10, 6)],

    [(7, 5), (3, 5)],
    #
    [(2, 3), (4, 6)],
    [(7, 8), (9, 11)],
    #
    [(5, 1), (9, 5)],
    [(2, 1), (10, 5)]
]

paral = []
fig = plt.figure()
for p in points:
    plt.plot([p[0][0], p[1][0]], [p[0][1], p[1][1]], color='gray')

for i in range(len(points) - 1):
    for j in range(1, len(points)):
        if points[i] != points[j] and is_paral(*points[i], *points[j]):
            in_list1 = [*points[i], *points[j]] in paral
            in_list2 = [*points[j], *points[i]] in paral
            if not in_list1 and not in_list2:
                paral.append([*points[i], *points[j]])

cmap = plt.get_cmap('jet_r')
N = len(paral)
for num, p in enumerate(paral):
    color = cmap(float(num)/N)
    plt.plot([p[0][0], p[1][0]], [p[0][1], p[1][1]], color=color)
    plt.plot([p[2][0], p[3][0]], [p[2][1], p[3][1]], color=color)


fig.suptitle(
    'Пары параллельных отрезков: {}'.format(len(paral)),
    fontsize=15
)
plt.show()
