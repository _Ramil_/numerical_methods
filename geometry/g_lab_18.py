"""
doc: http://steve.hollasch.net/cgindex/geometry/ptintet.html
V1 = (x1, y1, z1)
V2 = (x2, y2, z2)
V3 = (x3, y3, z3)
V4 = (x4, y4, z4)

P = (x, y, z).

     |x1 y1 z1 1|
D0 = |x2 y2 z2 1|
     |x3 y3 z3 1|
     |x4 y4 z4 1|

     |x  y  z  1|
D1 = |x2 y2 z2 1|
     |x3 y3 z3 1|
     |x4 y4 z4 1|

     |x1 y1 z1 1|
D2 = |x  y  z  1|
     |x3 y3 z3 1|
     |x4 y4 z4 1|

     |x1 y1 z1 1|
D3 = |x2 y2 z2 1|
     |x  y  z  1|
     |x4 y4 z4 1|

     |x1 y1 z1 1|
D4 = |x2 y2 z2 1|
     |x3 y3 z3 1|
     |x  y  z  1|

Если знак Di равен D0, то точка Р лежит внутри грани i
Если Р лежит внутри всех 4 граней, то она лежит внутри тетраэдр
"""


from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

A = np.array([3, 1, 1])
B = np.array([1, 3, 1])
C = np.array([1, 1, 2])
D = np.array([1, 4, 1])

# A = np.array([3, 0, 0])
# B = np.array([0, 3, 0])
# C = np.array([0, 0, 3])
# D = np.array([0, 0, 0])


fig = plt.figure()
ax = fig.gca(projection='3d')

ax.plot([A[0], B[0]], [A[1], B[1]], [A[2], B[2]], color='red')
ax.plot([D[0], B[0]], [D[1], B[1]], [D[2], B[2]], color='red')
ax.plot([D[0], A[0]], [D[1], A[1]], [D[2], A[2]], color='red')

ax.plot([C[0], B[0]], [C[1], B[1]], [C[2], B[2]], color='red')
ax.plot([C[0], A[0]], [C[1], A[1]], [C[2], A[2]], color='red')
ax.plot([C[0], D[0]], [C[1], D[1]], [C[2], D[2]], color='red')


def is_in_tetrahedron_by_det(a, b, c, d, p):

    D0 = np.array(
        [[a[0], a[1], a[2], 1],
        [b[0], b[1], b[2], 1],
        [c[0], c[1], c[2], 1],
        [d[0], d[1], d[2], 1]]
    )
    D1 = np.array(
        [[p[0], p[1], p[2], 1],
        [b[0], b[1], b[2], 1],
        [c[0], c[1], c[2], 1],
        [d[0], d[1], d[2], 1]]
    )
    D2 = np.array(
        [[a[0], a[1], a[2], 1],
        [p[0], p[1], p[2], 1],
        [c[0], c[1], c[2], 1],
        [d[0], d[1], d[2], 1]]
    )
    D3 = np.array(
        [[a[0], a[1], a[2], 1],
        [b[0], b[1], b[2], 1],
        [p[0], p[1], p[2], 1],
        [d[0], d[1], d[2], 1]]
    )
    D4 = np.array(
        [[a[0], a[1], a[2], 1],
        [b[0], b[1], b[2], 1],
        [c[0], c[1], c[2], 1],
        [p[0], p[1], p[2], 1]]
    )
    det0 = np.linalg.det(D0)
    return np.sign(det0) == np.sign(np.linalg.det(D1)) and \
          np.sign(det0) == np.sign(np.linalg.det(D2)) and \
          np.sign(det0) == np.sign(np.linalg.det(D3)) and \
          np.sign(det0) == np.sign(np.linalg.det(D4))


def is_same_side(a, b, c, d, p):
    normal = np.cross(b - a, c - a)
    dot_v4 = np.dot(normal, d - a)
    dot_p = np.dot(normal, p - a)
    return np.sign(dot_v4) == np.sign(dot_p)


def is_in_tetrahedron(a, b, c, d, p):
    res_n = is_same_side(a, b, c, d, p) and \
           is_same_side(b, c, d, a, p) and \
           is_same_side(c, d, a, b, p) and \
           is_same_side(d, a, b, c, p)
    return res_n


points = np.random.uniform(0, 3, size=[50, 3])
_in, _out, _len = 0, 0, len(points)

for point in points:
    if is_in_tetrahedron(A, B, C, D, point):
        color = 'blue'
        _in += 1
    else:
        _out += 1
        color = 'green'
    ax.scatter(*point, color=color)

fig.suptitle(
    'Внутри: {} Снаружи: {} Всего: {}'.format(_in, _out, _len),
    fontsize=15
)
plt.show()
