import matplotlib.pyplot as plt
from math import fabs, sqrt

"""
  A1 * x + B1 * y = C1
  A2 * x + B2 * y = C2
  --------------------
  x = Dx/D
  y = Dy/D
  --------------------
  D = | A1 B1 |
      | A2 B2 |
  --------------------
  Dx = | C1 B1 |
       | C2 B2 |
  --------------------
  Dy = | A1 C1 |
       | A2 C2 |
"""


def distance(a, b, x):
    def line(p1, p2):
        A = (p1[1] - p2[1])
        B = (p2[0] - p1[0])
        C = (p1[0] * p2[1] - p2[0] * p1[1])
        return A, B, -C

    A, B, C = line(a, b)
    return fabs(A * x[0] + B * x[1] + C) / sqrt(A ** 2 + B ** 2)


points = [
    [(1, 1), (3, 4)],
    [(3, 4), (5, 4)],
    [(5, 4), (6, 2)],
    [(6, 2), (6, 0)],
    [(6, 0), (4, 0)],
    [(4, 0), (3, 1)],
    [(3, 1), (1, 1)]
]

point = [5, 1]

fig = plt.figure()

for p in points:
    plt.plot([p[0][0], p[1][0]], [p[0][1], p[1][1]], color='black')

plt.plot(point[0], point[1], marker='o')

dists = []
for line in points:
    dists.append({'line': line, 'dist': distance(*line, x=point)})

dist = sorted(dists, key=lambda x: x['dist'])[0]
p = dist['line']
plt.plot([p[0][0], p[1][0]], [p[0][1], p[1][1]], color='blue')
fig.suptitle(
    'Наименьшее расстояние до прямой: {}'.format(dist['dist']),
    fontsize=15
)
plt.show()
