import matplotlib.pyplot as plt

"""
  A1 * x + B1 * y = C1
  A2 * x + B2 * y = C2
  --------------------
  x = Dx/D
  y = Dy/D
  --------------------
  D = | A1 B1 |
      | A2 B2 |
  --------------------
  Dx = | C1 B1 |
       | C2 B2 |
  --------------------
  Dy = | A1 C1 |
       | A2 C2 |
"""


def get_intersect(a, b, c, d):
    def line(p1, p2):
        A = (p1[1] - p2[1])
        B = (p2[0] - p1[0])
        C = (p1[0]*p2[1] - p2[0]*p1[1])
        return A, B, -C

    L1 = line(a, b)
    L2 = line(c, d)

    D  = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x, y
    return None

points = [[(1, 1), (5, 4)], [(3,1), (3, 5)], [(1, 4), (6,2)], [(5, 1), (9, 5)]]

ints = []
for p in points:
    plt.plot([p[0][0], p[1][0]], [p[0][1], p[1][1]])
    

for i in range(len(points) - 1):
    for j in range(1, len(points)):
        p = get_intersect(*points[i], *points[j])
        if p:
            plt.plot(p[0], p[1], marker='o')
            ints.append(p)

ints = list(set(ints))
print("Point")

for i in ints:
    print(i)

print(len(ints))
plt.show()
