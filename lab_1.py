import matplotlib.pyplot as plt

"""
    y'' - 4y = -x
    0 <= x <= 1
    y(0) = 0; y(1) = 2
    
    ----|---|-------------------|----
        0   x1                  X = xn
"""


def build_x(start, stop, step):
    l = []
    r = start
    while r <= stop:
        l.append(r)
        r += step
    return l

p = lambda _: 4
f = lambda _: -1 * _

N = 20
a, b = 0.0, 1.0
h = (b - a) / N
A, B = 0.0, 2.0


def shot():
    x = build_x(a, b, h)
    y = [
        [A, A + h],  # y0, y1
        [A, A + h]
    ]

    def solve_y0(_y, i):
        return h ** 2 * f(x[i]) + p(x[i]) * _y[i] * h ** 2 + 2 * _y[i] - _y[i-1]

    def solve_y1(_y, i):
        return p(x[i]) * _y[i] * h ** 2 + 2 * _y[i] - _y[i-1]

    for i in range(1, N):
        y[0].append(solve_y0(y[0], i))
        y[1].append(solve_y1(y[1], i))

    C = (B - y[0][N]) / y[1][N]
    Y = lambda k: y[0][k] + C * y[1][k]
    return x, [Y(i) for i in range(N)]


def run():
    x = build_x(a, b, h)
    C = [0]
    U = [a]
    y = []

    for i in range(N - 1):
        C1 = 1.0 / (2 + p(x[i+1]) * h ** 2 - C[i])
        U1 = C1 * (U[i] - f(x[i+1]) * h ** 2)
        C.append(C1)
        U.append(U1)
        y.append(0)

    y.append(B)
    for i in range(N - 2, 0, -1):
        y[i] = C[i] * y[i + 1] + U[i]
    return x, y

plt.subplot(211)
plt.grid(True)
plt.title('Метод стрельбы')
plt.plot(*shot())

plt.subplot(212)
plt.grid(True)
plt.title('Метод прогонки')
plt.plot(*run())

plt.show()
