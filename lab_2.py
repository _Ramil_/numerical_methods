"""
   f(x, y) = x ^ 3 + y ^ 4
   a(x, y) = 1
   b(x, y) = 2
   c(x, y) = 1
   d(x, y) = -2
   g(x, y) = 1
   F(x) = 3
   a = 1, b = 7
   G = {(x, y): a < x < b, 0 < y < F(x)}
   N = 50
"""

N = 50
n, m = 5, 6
a = lambda: 1
b = lambda: 2
c = lambda: 1
d = lambda: -2
g = lambda: 1

a_b, b_b, c_b, d_b = 1, 7, 0, 3

h = (b_b - a_b) / n
t = (d_b - c_b) / m
X = lambda i: a_b + (i / n) * (b_b - a_b)
Y = lambda j: c_b + (j / m) * (d_b - c_b)
u = lambda x, y: x ** 3 + y ** 4
F = lambda x, y: a() * 6 * x + \
                 b() * 12 * (y ** 2) + \
                 c() * 3 * (x ** 2) + \
                 d() * 4 * (y ** 3) + \
                 g() * u(x, y)

U = [[0 for j in range(m + 1)] for i in range(n + 1)]
default = [[u(X(i), Y(j)) for j in range(m + 1)] for i in range(n + 1)]
in_point = [
    [(round(X(i), 1), round(Y(j), 1), round(u(X(i), Y(j)), 1)) for j in range(m + 1)]
    for i in range(n + 1)
]

for i in range(n + 1):
    U[i][0] = u(X(i), c_b)
    U[i][m] = u(X(i), d_b)

for i in range(m + 1):
    U[0][i] = u(a_b, Y(i))
    U[n][i] = u(b_b, Y(i))

A = lambda i, j: (a() / (h ** 2)) + (c() / (2 * h))
B = lambda i, j: (a() / (h ** 2)) - (c() / (2 * h))
C = lambda i, j: (b() / (t ** 2)) + (d() / (2 * t))
D = lambda i, j: (b() / (t ** 2)) - (d() / (2 * t))
E = lambda i, j: (2 * a() / (h ** 2)) + (2 * b() / (t ** 2)) - g()


def calculate(matrix, count):
    k = 0
    while k < count:
        for i in range(1, n):
            for j in range(1, m):
                matrix[i][j] = (A(i, j) / E(i, j)) * U[i + 1][j] + \
                           (B(i, j) / E(i, j)) * U[i - 1][j] + \
                           (C(i, j) / E(i, j)) * U[i][j + 1] + \
                           (D(i, j) / E(i, j)) * U[i][j - 1] - \
                           (F(X(i), Y(j)) / E(i, j))
        k += 1
    return matrix


def pprint(func):
    print('\n'.join(
        [" ".join([
            '{:10}'.format(str(func(i, j)))
            for j in range(m + 1)])
            for i in range(n + 1)])
    )
    print("======================================================================")


U = calculate(U, N)
pprint(lambda i, j: round(default[i][j], 2))
pprint(lambda i, j: round(U[i][j], 2))
pprint(lambda i, j: round(U[i][j] - default[i][j], 2))
