"""
   f(x, y) = sin(x) * cos(y)
   a(x, y) = 1
   b(x, y) = 1
   c(x, y) = 0
   d(x, y) = 0
   g(x, y) = 0
   G = [0, pi] x [0, 1]
   N = 10
"""

from math import sin, cos, pi

N = 10
n, m = 5, 6
a = lambda: 1
b = lambda: 1
c = lambda: 0
d = lambda: 0
g = lambda: 0
u = lambda x, y: sin(x) * cos(y)

a_b = pi
d_b = 1
h = a_b / n
t = d_b / m

X = lambda i: i * h
Y = lambda j: j * t

F = lambda x, y: 0
w = lambda i: sin(X(i))
ksi = lambda i: 0

default = [[u(X(i), Y(j)) for j in range(m + 1)] for i in range(n + 1)]
U = [[0 for j in range(m + 1)] for i in range(n + 1)]

A = lambda i, j: -(b() / (t ** 2)) + (d() / (2 * t))
B = lambda i, j: -(b() / (t ** 2)) - (d() / (2 * t))
C = lambda i, j: (a() / (h ** 2)) + (c() / (2 * h))
D = lambda i, j: (a() / (h ** 2)) - (c() / (2 * h))
E = lambda i, j: (-2 * a() / (h ** 2)) + (2 * b() / (t ** 2)) + g()

for i in range(n + 1):
    U[i][0] = w(i)
    U[i][1] = (F(X(i), 0) +
               2 * t * B(i, 0) * ksi(X(i)) -
               C(i, 0) * w(i + 1) -
               D(i, 0) * w(i - 1) -
               E(i, 0) * w(i)) / (A(i, 0) + B(i, 0))

for i in range(m + 1):
    U[0][i] = 0
    U[n][i] = 0


def pprint(func):
    print('\n'.join(
        [" ".join([
            '{:10}'.format(str(func(i, j)))
            for j in range(m + 1)])
            for i in range(n + 1)])
    )
    print("======================================================================")


def calculate(matrix, count):
    k = 0
    while k < count:
        for i in range(1, n):
            for j in range(1, m):
                matrix[i][j + 1] = F(X(i), Y(j)) - (1 / A(i, j)) * (
                    B(i, j) * U[i][j - 1] + C(i, j) * U[i + 1][j] +
                    D(i, j) * U[i - 1][j] + E(i, j) * U[i][j]
                )
        k += 1
    return matrix


U = calculate(U, N)
pprint(lambda i, j: round(default[i][j], 2))
pprint(lambda i, j: round(U[i][j], 10))
pprint(lambda i, j: round(U[i][j] - default[i][j], 10))
