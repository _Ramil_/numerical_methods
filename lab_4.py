from math import cos, pi, e

n = 100
a_b = 0
b_b = pi
a_0 = lambda: 0
a_1 = lambda: 0
b_0 = lambda: 0
b_1 = lambda: 0

h = (b_b - a_b) / n
l = h
m = n + 1

X = lambda i: a_b + (i - 0.5) * h
T = lambda j: j * l
phi = lambda i: cos(X(i))
nu = lambda: 1

A = 1
B = 1 + ((h ** 2) / (2 * l * nu()))
C = 1

__U = lambda x, t: cos(x) * (e ** (-t))

R = 1
S = 0


def built_P_j():
    P = [1]
    for i in range(1, n + 2):
        P.append(1 / (2 * B - P[i - 1]))
    return P


def built_D_j(u_str):
    D = [-(h ** 2 / (l * nu())) * u_str[0]]
    for i in range(1, n + 2):
        D.append((-(h ** 2) / (l * nu())) * u_str[i])
    return D


def built_Q_j(P, D):
    Q = [0]
    for i in range(1, n + 2):
        Q.append(((C * Q[i - 1] - D[i - 1]) / A) * P[i])
    return Q


U = [[0 for i in range(n + 2)] for j in range(m)]
_U = [[__U(X(i), T(j)) for i in range(n + 2)] for j in range(m)]

for i in range(n + 2):
    U[0][i] = phi(i)

for j in range(1, m):
    P = built_P_j()
    D = built_D_j(U[j - 1])
    Q = built_Q_j(P, D)
    U[j][n + 1] = Q[n + 1] / (1 - P[n + 1])
    U[j][n] = U[j][n + 1]
    for i in range(n - 1, -1, -1):
        U[j][i] = P[i + 1] * U[j][i + 1] + Q[i + 1]


def pprint(func):
    print('\n'.join(
        [" ".join([
            '{:10}'.format(str(func(i, j)))
            for j in range(n + 2)])
            for i in range(m)])
    )
    print("=" * 80)


pprint(lambda i, j: round(_U[i][j], 4))
pprint(lambda i, j: round(U[i][j], 4))
pprint(lambda i, j: round(U[i][j] - _U[i][j], 4))
