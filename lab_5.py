N = 5
step = 1000
alpha = 0.01

a = [[1 for i in range(N)] for j in range(N)]
for i in range(N):
    for j in range(N):
        if j < i:
            a[i][j] = 0
b = [N - i for i in range(N)]
x = [0 for i in range(N)]
x_ = [0 for i in range(N)]

print('Общий случай===========')
print('Матрица:')
print(a)
print('===========')

for l in range(step):
    for k in range(N):
        xk = x[k] - alpha * sum(
            [(sum([a[i][j] * x[j] for j in range(N)]) - b[i]) * a[i][k] for i in range(N)])
        x_[k] = xk
    x = x_
print('Результат:')
print(x)
print('Пример для проверки=======')
eps = 1.00001
a = [[1 for i in range(3)] for j in range(3)]
a[1][0] += eps
a[2][2] = 0
print('Матрица:')
print(a)
print('===========')

b = [3, 3 + eps, 2]
x = [0 for i in range(3)]
x_ = [0 for i in range(3)]

for l in range(step):
    for k in range(3):
        xk = x[k] - alpha * sum([(sum([a[i][j] * x[j] for j in range(3)]) - b[i]) * a[i][k] for i in range(3)])
        x_[k] = xk
    x = x_
print('Результат:')
print(x)
