"""
    Часть № 1
"""
A = 0
B = 2
N = 200
H = (B - A) / N
F = lambda x: x ** 3


def generate_points(a, b, n):
    x = []
    h = (b - a) / n
    for i in range(n + 1):
        x.append(a + i * h)
    return x


def rectangles(x, h):
    return h * sum([F(0.5 * (x[i] + x[i + 1])) for i in range(N)])


def trapeziums(x, a, b, h):
    return h * (0.5 * (F(b) + F(a)) + sum([F(x[i]) for i in range(1, N)]))


def simpson(x, a, b, h):
    return h / 6 * (
        F(b) +
        F(a) +
        2 * (sum([F(x[i]) for i in range(1, N)])) +
        4 * (sum([F((x[i + 1] + x[i]) / 2) for i in range(N)]))
    )

X = generate_points(A, B, N)

print('Метод прямоугольников: ', rectangles(X, H))
print('Метод трапеций: ', trapeziums(X, A, B, H))
print('Метод Cимпсона: ', simpson(X, A, B, H))

"""
    Часть № 2
"""
M = 200
F2 = lambda x, y: x
PHI = lambda x: 0
PSI = lambda x: -x ** 2 + 2 * x + 1
EPS = lambda x, i: (x[i] + x[i + 1]) / 2


def generate_Y(x, n, m):
    y = []
    for i in range(n):
        tmp = []
        for j in range(m + 1):
            tmp.append(
                PHI(EPS(x, i)) + (PSI(EPS(x, i)) - PHI(EPS(x, i))) * j / m
            )
        y.append(tmp)
    return y


def double_rectangles(x, y, h, n, m):
    mesh = []
    for i in range(n):
        _sum = sum([F2(EPS(x, i), (y[i][j] + y[i][j + 1]) / 2) for j in range(m)])
        mesh.append(PSI(EPS(x, i)) - PHI(EPS(x, i)) / m * _sum)
    return h * sum(mesh)
Y = generate_Y(X, N, M)
print('Значение двойного интеграла: ', double_rectangles(X, Y, H, N, M))
