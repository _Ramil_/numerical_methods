import math as m

R = 2
n = 10
a_b = -R / (2 ** 0.5)
b_b = R / (2 ** 0.5)

phi = lambda t: R * m.cos(m.atan2((R ** 2 - t ** 2) ** 0.5, t))
psi = lambda t: R * m.sin(m.atan2((R ** 2 - t ** 2) ** 0.5, t))
hi = lambda t: 0
make_t = lambda a, b, n: [a + i * (b - a) / n for i in range(n + 1)]
t = make_t(a_b, b_b, n)

result = sum(
    [((phi(t[i + 1]) - phi(t[i])) ** 2 +
      (psi(t[i + 1]) - psi(t[i])) ** 2 +
      (hi(t[i + 1]) - hi(t[i])) ** 2) ** 0.5 for i
     in range(n)])
print('I:', result)


make_x = lambda a, b, n: [a + i * (b - a) / (2 * n) for i in range(2 * n + 1)]
y = lambda x: (R ** 2 - x ** 2) ** 0.5
f = lambda a, b, x: 0.5 * (
    (2 * a * x + b) *
    (1 + (2 * a * x + b) ** 2) ** 0.5
    + m.log(2 * a * x + b + (1 + (2 * a * x + b) ** 2) ** 0.5)
)
a, b = [], []
x = make_x(a_b, b_b, n)

for i in range(n):
    phi1 = (y(x[2 * i + 1]) - y(x[2 * i])) / (x[2 * i + 1] - x[2 * i])
    phi2 = (y(x[2 * i + 2]) - y(x[2 * i])) / (x[2 * i + 2] - x[2 * i])

    a.append((phi2 - phi1) / (x[2 * i + 2] - x[2 * i + 1]))
    b.append((phi1 * (x[2 * i + 2] + x[2 * i]) -
              phi2 * (x[2 * i + 1] + x[2 * i])) /
             (x[2 * i + 2] - x[2 * i + 1]))

s = 0

for i in range(n):
    if a[i] == 0:
        s += (1 + b[i] ** 2) ** 0.5 * (x[2 * i + 2] - x[2 * i])
    else:
        s += 1 / (2 * a[i]) * (f(a[i], b[i], x[2 * i + 2]) - f(a[i], b[i], x[2 * i]))
print('II:', s)
