from collections import namedtuple
import math

n = 200
m = 200
R = 2

a_b = 0
b_b = 2

c = -R / (2 ** 0.5)
d = R / (2 ** 0.5)
make_x = lambda a, b, n: [a + i * (b - a) / n for i in range(n + 1)]
make_y = lambda c, d, m: [c + i * (d - c) / m for i in range(m + 1)]
f = lambda x, y: (R ** 2 - y ** 2) ** 0.5

x = make_x(a_b, b_b, n)
y = make_y(c, d, m)


def task_one():
    h = (b_b - a_b) / n
    t = (d - c) / m
    s = 0
    for i in range(n):
        for j in range(m):
            s += (
                  (1 +
                   ((f(x[i + 1], y[j]) - f(x[i], y[j])) / (x[i + 1] - x[i])) ** 2 +
                   ((f(x[i + 1], y[j + 1]) - f(x[i + 1], y[j])) / (y[j + 1] - y[j])) ** 2
                   ) ** 0.5 * t * h +

                  (1 +
                   ((f(x[i + 1], y[j + 1]) - f(x[i], y[j + 1])) / (x[i + 1] - x[i])) ** 2 +
                   ((f(x[i], y[j + 1]) - f(x[i], y[j])) / (y[j + 1] - y[j])) ** 2
                   ) ** 0.5 * t * h)
    return 0.5 * s


print(task_one())
print(math.pi * R)
print("=" * 15)

phi = lambda u, v: R * math.cos(u) * math.sin(v)
psi = lambda u, v: R * math.sin(u) * math.cos(v)
hi = lambda u, v: R * math.cos(v)

a, b = 0, 2 * math.pi
c, d = 0, 0.5 * math.pi
Point = namedtuple('Point', ['x', 'y', 'z'])
u = make_x(a, b, n)
v = make_y(c, d, m)


def task_two():
    def sigma(A, B, C):
        return 0.5 * (
            ((B.y - A.y) * (C.z - A.z) - (C.y - A.y) * (B.z - A.z)) ** 2 +
            ((B.x - A.x) * (C.z - A.z) - (C.x - A.x) * (B.z - A.z)) ** 2 +
            ((B.x - A.x) * (C.y - A.y) - (C.x - A.x) * (B.y - A.y)) ** 2
        ) ** 0.5

    B = []
    for i in range(n + 1):
        B.append([
            Point(phi(u[i], v[j]), psi(u[i], v[j]), hi(u[i], v[j]))
            for j in range(m + 1)
        ])

    s = 0
    for i in range(n):
        for j in range(m):
            s += (
                sigma(B[i][j], B[i + 1][j], B[i + 1][j + 1]) +
                sigma(B[i][j], B[i][j + 1], B[i + 1][j + 1])
            )
    return s

print(task_two())
print(2 * math.pi * R ** 2)
