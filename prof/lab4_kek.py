import numpy as np


def solve(xi, tj):
    return np.power(np.e, -tj) * np.cos(xi)


def phi(xi):
    return np.cos(xi)


def alpha_zero(tj):
    return 0


def alpha_one(tj):
    return 0


def beta_zero(tj):
    return 0


def beta_one(tj):
    return 0


def mu(x, y):
    return 1


def bim(xi, tj, h, l):
    return 1 + h ** 2 / (2 * l * mu(xi, tj) ** 2)


def dim(xi, tj, h, l, ui_m_minus_one):
    return - h ** 2 / (l * mu(xi, tj) ** 2) * ui_m_minus_one


def p0m(tj, h):
    return (2 - h * alpha_zero(tj)) / (2 + h * alpha_zero(tj))


def q0m(tj, h):
    return -2 * h * alpha_one(tj) / (2 + h * alpha_zero(tj))


def rm(tj, h):
    return (2 - h * beta_zero(tj)) / (2 + h * beta_zero(tj))


def sm(tj, h):
    return -2 * h * beta_one(tj) / (2 + h * beta_zero(tj))


ax, bx = 0, np.pi
N = 1000
M = 1000
h = (bx - ax) / N
l = h

X = list(map(lambda i: ax + h * (i + 0.5), np.arange(-1, N + 1, 1)))
T = list(map(lambda j: l * j, range(M)))

u = np.zeros((M, N + 2))
u[0, :] = np.array(list(map(lambda xi: phi(xi), X)))

am = cm = 1


for j in range(1, M):
    pj = [p0m(T[j], h)]
    qj = [q0m(T[j], h)]

    for i in range(1, N + 2):
        bij = bim(X[i - 1], T[j], h, l)
        dij = dim(X[i - 1], T[j], h, l, u[j - 1, i - 1])
        pj.append(am / (2 * bij - cm * pj[i - 1]))
        qj.append((cm * qj[i - 1] - dij) / am * pj[i])

    u[j, N + 1] = qj[N + 1] / (1 - pj[N + 1])
    # u[j, N] = u[j, N + 1]
    u[j, N] = rm(T[j], h) * u[j, N + 1] + sm(T[j], h)
    for i in range(N - 1, -1, -1):
        u[j, i] = pj[i + 1] * u[j, i + 1] + qj[i + 1]


solution = u.copy()
for j in range(M):
    for i in range(N + 2):
        solution[j, i] = solve(X[i], T[j])


print(np.abs(solution - u).max())
