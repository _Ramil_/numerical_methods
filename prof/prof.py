# import sys
#
# with open('input.txt') as f:
#     lines = f.readlines()
#
# array = [int(i) for i in lines[1].split(' ')]
# len_array = len(array)
# abs_min = sys.maxsize
#
# for i in range(len_array):
#     for j in range(1, len_array):
#         if i != j:
#             tmp = abs(array[i] - array[j])
#             if tmp < abs_min:
#                 abs_min = tmp
#
# print(abs_min)


# def has_duplicates(elements):
#     for i in range(0, len(elements) - 1):
#         if elements[i] == elements[i + 1]:
#             return True
#     return False
#
#
# def permutations(elements):
#     if len(elements) <= 1:
#         return elements
#
#     ret = []
#     for i, item in enumerate(elements):
#         for jan in permutations(elements[:i] + elements[i + 1:]):
#             ret.append(item + jan)
#     return [el for el in ret if not has_duplicates(el)]
#
#
# with open('input2.txt') as f:
#     lines = f.readlines()
#
# letters = []
# A, B, C = lines[0].split(' ')
# for i in range(int(A)):
#     letters.append('a')
# for i in range(int(B)):
#     letters.append('b')
# for i in range(int(C)):
#     letters.append('c')
# result = permutations(letters)
#
# if len(result) > 1:
#     print(result[0])
# else:
#     print('impossible')

def is_in_line(substring, string):
    i = 0
    last_found = -1
    while True:
        last_found = string.find(substring, last_found + 1)
        if last_found == -1:
            break
        i += 1
    return i


def palindromes(text):
    text = text.lower()
    results = []

    for i in range(len(text)):
        for j in range(0, i):
            chunk = text[j:i + 1]

            if chunk == chunk[::-1] and is_in_line(chunk, text) >= 2:
                results.append(len(chunk))

    return results

with open('input.txt') as f:
    lines = f.readlines()
results = palindromes(lines[0])
print(sorted(results, reverse=True)[0])
