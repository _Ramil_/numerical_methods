import math
from triangulation.point import Point

A = -1
B = 1
N = 10
M = 10
node = 24


def phi(x):
    return x - 1


def psi(x):
    return x ** 2 + 2

points = Point.build(A, B, phi, psi, N, M)
Point.points_to_file(points)
Point.triangles_to_file(N, M)

points = Point.get_points_from_file()
triangles = Point.get_triangles_from_file()
area, min_sin = Point.calculate_area_and_sin_from_file(points, triangles)
border = Point.get_border(points, triangles)

plot = Point.make_plot(points, triangles, border)
plot.subplots_adjust(left=0.3)
plot.gcf().text(
    0.02,
    0.02,
    'Минимальный синус: {}({})\n'
    'Площадь: {}\n'
    'Количество граничных точек: {}\n'
    'Количество треугольников с вершиной {}: {}\n'.format(
        round(min_sin, 2),
        round(math.degrees(math.asin(min_sin)), 2),
        round(area, 2),
        len(border),
        node,
        Point.get_neighbours_count(node, triangles)
    )
)
mng = plot.get_current_fig_manager()
mng.full_screen_toggle()
plot.show()
