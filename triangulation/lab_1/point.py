import matplotlib.pyplot as plt


class Point:
    def __init__(self, x: float=0, y: float=0, number=0):
        self.x = x
        self.y = y
        self.number = number

    def distance_from_other_point(self, p):
        return ((self.x - p.x) ** 2 + (self.y - p.y) ** 2) ** 0.5

    @classmethod
    def build(cls, a, b, phi, psi, n, m):
        """
        Построение точек для области
        :param a: начало
        :param b: конец
        :param phi: функция
        :param psi: функция
        :param n: количество точек по Х
        :param m: количество точек по У
        :return: [Point_1, Point_2, ..., Point_N]
        """
        points = []
        number = 0
        for j in range(m + 1):
            for i in range(n + 1):
                x = a + i * (b - a) / n
                y = phi(x) + j * (psi(x) - phi(x)) / m
                points.append(Point(x, y, number))
                number = number + 1
        return points

    @classmethod
    def points_to_file(cls, points, filename='data/points.pnt'):
        file = open(filename, 'w')
        for point in points:
            file.write('{} {}\n'.format(str(point.x), str(point.y)))
        file.close()

    @classmethod
    def triangles_to_file(cls, n, m, filename='data/triangles.pnt'):
        """
        Триангуляция точек
        2 точки
        0 1 2
        1 2 3
        """
        file = open(filename, 'w')
        for i in range(0, n):
            for j in range(0, m):
                file.write('{} {} {}\n'.format(
                    str(i + 1 + j * (n + 1)),
                    str(i + (j + 1) * (n + 1)),
                    str(i + j * (n + 1))
                ))
                file.write('{} {} {}\n'.format(
                    str(i + 1 + j * (n + 1)),
                    str(i + (j + 1) * (n + 1)),
                    str(i + 1 + (j + 1) * (n + 1))
                ))
        file.close()

    @classmethod
    def get_points_from_file(cls, filename='data/points.pnt'):
        file = open(filename, 'r')
        points = []
        number = 0

        for line in file.readlines():
            l = line.split()
            if not len(l) > 1:
                continue
            points.append(cls(float(l[0]), float(l[1]), number))
            number = number + 1
        file.close()
        return points

    @classmethod
    def get_triangles_from_file(cls, filename='data/triangles.pnt'):
        file = open(filename, 'r')
        triangles = []
        for line in file.readlines():
            l = line.split()
            if not len(l) > 1:
                continue
            triangles.append([int(l[0]), int(l[1]), int(l[2])])
        file.close()
        return triangles

    @classmethod
    def calculate_area_and_sin_from_file(cls, points, triangles):
        area = 0
        min_sin = 1
        for triangle in triangles:
            _points = [
                points[triangle[0]],
                points[triangle[1]],
                points[triangle[2]]
            ]
            area += cls.area(*_points)
            min_sin = min(cls.min_sin(*_points), min_sin)
        return area, min_sin

    @classmethod
    def area(cls, point1, point2, point3):
        """
        Площадь треугольника
        :param point1: Вершина 1
        :param point2: Вершина 2
        :param point3: Вершина 3
        :return: float
        """
        a = point1.distance_from_other_point(point2)
        b = point2.distance_from_other_point(point3)
        c = point3.distance_from_other_point(point1)
        p = (a + b + c) / 2
        return (p * (p - a) * (p - b) * (p - c)) ** 0.5

    @classmethod
    def min_sin(cls, point1, point2, point3):
        """
        Минимальный синус треугольника
        :param point1: Вершина 1
        :param point2: Вершина 2
        :param point3: Вершина 3
        :return: float
        """
        a = point1.distance_from_other_point(point2)
        b = point2.distance_from_other_point(point3)
        c = point3.distance_from_other_point(point1)
        if not a or not b or not c:
            return 0
        sin_alpha = (1 - ((b ** 2 + c ** 2 - a ** 2) / (2 * b * c)) ** 2) ** 0.5
        sin_beta = (1 - ((b ** 2 + a ** 2 - c ** 2) / (2 * b * a)) ** 2) ** 0.5
        sin_gamma = (1 - ((a ** 2 + c ** 2 - b ** 2) / (2 * a * c)) ** 2) ** 0.5
        return min(sin_alpha, sin_beta, sin_gamma)

    @classmethod
    def get_neighbours_count(cls, number, triangles):
        return sum([1 for triangle in triangles if number in triangle])

    @classmethod
    def get_border(cls, points, triangles):
        border = []
        for point in points:
            count = cls.get_neighbours_count(point.number, triangles)
            count <= 3 and border.append(point)
        return border

    @classmethod
    def make_plot(cls, points, triangles, border=None):
        if border is None:
            border = []
        x = [i.x for i in points]
        y = [i.y for i in points]

        plt.plot(x, y, 'ro')
        for point in points:
            color = 'red' if point in border else 'black'
            plt.annotate(
                point.number,
                xy=(point.x, point.y),
                color=color
            )
        plt.triplot(x, y, triangles)
        return plt

    def __repr__(self):
        return '{}:{}:{}'.format(
            self.x,
            self.y,
            self.number
        )
