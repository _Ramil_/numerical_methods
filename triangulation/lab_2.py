import numpy as np
import scipy.linalg as alg
import matplotlib.pyplot as plt

a = 0
b = 2
c = 0
d = 2

n = 10
m = 5
h = (b - a) / n
t = (d - c) / m

built_x = lambda a, b, n: [a + i * (b - a) / n for i in range(n + 1)]
built_y = lambda c, d, m: [c + i * (d - c) / m for i in range(m + 1)]
u = lambda x, y: 0.25 * (x ** 2 + y ** 2)
u1 = lambda x, y: x * y


def built_t(n, m):
    T = []
    for i in range(n):
        for j in range(m):
            T.append([(m + 1) * i + j, (m + 1) * i + j + 1, (m + 1) * (i + 1) + j + 1])
            T.append([(m + 1) * i + j, (m + 1) * (i + 1) + j + 1, (m + 1) * (i + 1) + j])
    return T


print('=' * 15)
X = built_x(a, b, n)
Y = built_y(c, d, m)

U = []
for i in range(n + 1):
    for j in range(m + 1):
        U.append(u1(X[i], Y[j]))

T = built_t(n, m)
print('=' * 15)
print("II")


def S(x1, x2, x3, y1, y2, y3):
    return abs(0.5 * alg.det([[1, x1, y1], [1, x2, y2], [1, x3, y3]]))

result = sum([
    (U[T[k][0]] + U[T[k][1]] + U[T[k][2]]) *

    S(X[T[k][0] // (m + 1)], X[T[k][1] // (m + 1)],
      X[T[k][2] // (m + 1)], Y[T[k][0] % (m + 1)],
      Y[T[k][1] % (m + 1)], Y[T[k][2] % (m + 1)]) / 3
    for k in range(len(T))])
print(result)

print('=' * 15)
print("III")

U = []
for i in range(n + 1):
    for j in range(m + 1):
        U.append(u(X[i], Y[j]))

result = 0


def solve_system(A, B):
    return [alg.det([B, A[:, 1], A[:, 2]]) / alg.det(A),
            alg.det([A[:, 0], B, A[:, 2]]) / alg.det(A),
            alg.det([A[:, 0], A[:, 1], B]) / alg.det(A)]

for k in range(len(T)):
    A = np.array([[X[T[k][0] // (m + 1)], Y[T[k][0] % (m + 1)], 1],
                  [X[T[k][1] // (m + 1)], Y[T[k][1] % (m + 1)], 1],
                  [X[T[k][2] // (m + 1)], Y[T[k][2] % (m + 1)], 1]])
    B = np.array([U[T[k][0]], U[T[k][1]], U[T[k][2]]])

    u_func = solve_system(A, B)
    result += ((u_func[0] ** 2 + u_func[1] ** 2) *

               S(X[T[k][0] // (m + 1)], X[T[k][1] // (m + 1)],
                 X[T[k][2] // (m + 1)], Y[T[k][0] % (m + 1)],
                 Y[T[k][1] % (m + 1)], Y[T[k][2] % (m + 1)]))

print(result)
print('=' * 15)
print("IV")


def built_x_bad(a, b, n):
    x = [a]
    for i in range(1, n + 1):
        x.append(round(x[i - 1] + np.random.random() * (b - x[i - 1]) / 2, 4))
    return x


def built_y_bad(c, d, m):
    y = [c]
    for i in range(1, m + 1):
        y.append(round(y[i - 1] + np.random.random() * (d - y[i - 1]) / 2, 4))
    return y


X = built_x_bad(a, b, n)
Y = built_y_bad(c, d, m)

x = [X[i // (m + 1)] for i in range((n + 1) * (m + 1))]
y = [Y[i % (m + 1)] for i in range((n + 1) * (m + 1))]

plt.figure(1)
plt.subplot(211)
for i in range((n + 1) * (m + 1)):
    plt.annotate(i, xy=(x[i], y[i]))
plt.triplot(x, y, T)

neighbors = [set() for i in range((n + 1) * (m + 1))]

for i in range(len(T)):
    neighbors[T[i][0]].add(T[i][1])
    neighbors[T[i][0]].add(T[i][2])
    neighbors[T[i][1]].add(T[i][0])
    neighbors[T[i][1]].add(T[i][2])
    neighbors[T[i][2]].add(T[i][1])
    neighbors[T[i][2]].add(T[i][0])

while True:
    x_ = [x[i] for i in range((n + 1) * (m + 1))]
    y_ = [y[i] for i in range((n + 1) * (m + 1))]
    for i in range(len(neighbors)):
        if i // (m + 1) != 0 and i % (m + 1) != 0 and i // (m + 1) != n and i % (m + 1) != m:
            x_[i] = sum([x[j] for j in neighbors[i]]) / len(neighbors[i])
            y_[i] = sum([y[j] for j in neighbors[i]]) / len(neighbors[i])

    if x == x_ and y == y_:
        break
    x = x_
    y = y_

plt.subplot(212)
for i in range((n + 1) * (m + 1)):
    plt.annotate(i, xy=(x[i], y[i]))
plt.triplot(x, y, T)
mng = plt.get_current_fig_manager()
mng.full_screen_toggle()
plt.show()
